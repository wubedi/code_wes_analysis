#!/bin/bash

# Software: 	trimmomatic
# Source:		http://www.usadellab.org/cms/?page=trimmomatic
# Reference:	Bolger, A. M., Lohse, M., & Usadel, B. (2014). Trimmomatic: A flexible 
				# trimmer for Illumina Sequence Data. Bioinformatics, btu170.
# Objective:	Remove adapter sequences in reads, remove stretches of low quality in reads, remove 
				# start and end bases with low quality, remove reads with length under specified
				# threshold

java -Xmx4g -jar trim.jar PE \
	"$INPUT_R1" \
	"$INPUT_R2" \
	"${OUTPUT}_R1_P_qc.fastq.gz" \
	"${OUTPUT}_R1_U_qc.fastq.gz" \
	"${OUTPUT}_R2_P_qc.fastq.gz" \
	"${OUTPUT}_R2_U_qc.fastq.gz" \
	ILLUMINACLIP:"$TRUSEQ":2:30:10 \
	ILLUMINACLIP:"$NEXTERA:"2:30:10 \
	HEADCROP:15 \
	CROP:100 \
	LEADING:3 \
	TRAILING:3 \
	SLIDINGWINDOW:4:15 \
	MINLEN:75

### VARIABLES:
# $INPUT_R1:	name and path of unfiltered FASTQ files, read1
# $INPUT_R1:	name and path of unfiltered FASTQ files, read2 (paired read to R1) 
# $OUTPUT:		basename for filtered, trimmed FASTQ output files
# $TRUSEQ:		TEXT file containing Truseq adapter sequences
# $NEXTERA:		TEXT file containing Nextera adapter sequences

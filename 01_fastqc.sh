#!/bin/bash

# Software:		fastqc
# Source:		https://www.bioinformatics.babraham.ac.uk/projects/fastqc/
# Reference:	NA
# Objective:	Overview of quality in high throughput sequencing data, i.e. read length,
				# GC content, quality score distribution per base, nucleotide distribution,
				# adapter content and presence of adapter sequences or overrepresented K-mers

fastqc "$INPUT" -o "$OUTPUT"

### VARIABLES:
# $INPUT:		name and path of unfiltered FASTQ files
# $OUTPUT:		name and path for analysis output

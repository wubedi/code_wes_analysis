#!/bin/bash

# Software:		Genome Analysis Toolkit
# Source:		https://software.broadinstitute.org/gatk/
# Reference:	DePristo M. et al. framework for variation discovery and genotyping using 
				# next-generation DNA sequencing data. 2011 NATURE GENETICS 43:491-498 
# Objective:	recalibrate variant quality scores for SNPs supported by literature data and filter
				# SNPs accordingly

# STEP1: fit Gaussian mixture model for variant recalibration 
java -Xmx1g -jar GATK.jar \
	-T VariantRecalibrator \
	-R $REFERENCE \
	--input $INPUT \
	--resource:hapmap,known=false,training=true,truth=true,prior=15.0 $HAPMAP \
	--resource:omni,known=false,training=true,truth=false,prior=12.0 $OMNI \
	--resource:dbsnp,known=true,training=false,truth=false,prior=2.0 $DBSNP \
	--resource:1000G,known=false,training=true,truth=false,prior=10.0 $TG \
	-an QD \
	-an MQRankSum \
	-an ReadPosRankSum \
	-an FS \
	-an SOR \
	-an AF \
	-tranche 100.0 -tranche 99.9 -tranche 99.0 -tranche 95.0 -tranche 90.0 \
	--maxGaussians 4 \
	--mode SNP \
	--badLodCutoff -5 \
	-minNumBad 1000 \
	-recalFile $OUTPUT.snp.recal \
	-tranchesFile $OUTPUT.snp.tranches \
	-log $OUTPUT.snp.recal.log \
	-rscriptFile $OUTPUT.snp.plots.R

# STEP2: create new vcf file with recalibrated quality scores
java -Xmx1g -jar GATK.jar \
	-T ApplyRecalibration \
	-R $REFERENCE.fasta \
	--input $INPUT.vcf \
	--ts_filter_level 99 \
	-mode SNP \
	-recalFile $OUTPUT.snp.recal \
	-tranchesFile $OUTPUT.snp.tranches \
	-log $OUTPUT.snp.recal2.log \
	-o $OUTPUT.recalibrated.snp.vcf

### VARIABLES:
# $INPUT:		name and path of multi-sample VCF file
# $REFERENCE:	name and path to reference genome FASTA file
# $OUTPUT:		basename for output files as VCF file with recalibrated SNP quality scores
				# and mulitple log files

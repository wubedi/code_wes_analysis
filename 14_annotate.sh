#!/bin/bash

# Software:		annovar
# Source:		http://annovar.openbioinformatics.org/en/latest/
# Reference:	Yang H, Wang K. Genomic variant annotation and prioritization with ANNOVAR and
				# wANNOVAR Nature Protocols, 10:1556-1566, 2015
# Objective:	annotate variants with literature and functional data, predict variant 
				# functional consequence and impact

# STEP1: convert vcf to annovar input format 
convert2annovar.pl \
 	-format vcf4 $INPUT \
 	-outfile $OUTPUT.avinput \
 	-includeinfo \
 	-allsample \
 	-withfreq

# STEP2: perform annotation
table_annovar.pl $OUTPUT.avinput \
	$HUMANDB \
	-buildver hg19 \
	-out $OUTPUT.txt \
	-otherinfo \
	-remove \
	-protocol refGene,clinvar_20161128,caddindel,\
	dbnsfp31a_interpro,dbscsnv11,exac03,ljb26_all,revel,\
	targetScanS,wgRna,oreganno \
	-operation g,f,f,f,f,f,f,f,r,r,r \
	-nastring .

### VARIABLES:
# $INPUT:		multi-sample VCF file with recalibrated SNP and INDEL quality scores
# $HUMANDB:		path to storage location of annovar annotation databases
# $OUTPUT:		basename for output: annotated tab-delimited variant file
#!/bin/bash

# Software:		picard, SAMtools
# Source:		https://broadinstitute.github.io/picard/
# Reference:	NA
# Objective: 	mark PCR duplication artefacts



java -Xmx4g -jar picard.jar MarkDuplicates \
	I=$INPUT \
	O=$OUTPUT.bam \
	METRICS_FILE=$OUTPUT.metrics \
	VALIDATION_STRINGENCY=LENIENT

samtools view \
	-b \
	-L $TARGET \
	"$OUTPUT.bam" > "$OUTPUT.covered.bam"

samtools index "$OUTPUT.marked.covered.bam"

### VARIABLES:
# $INPUT:		name and path of mapped, sorted, compressed, merged BAM file
# $OUTPUT:		basename for output files including BAM file marked for duplicated reads and
				# filtered for seqeuences overlapping target
# $TARGET:		BED file with Target regions

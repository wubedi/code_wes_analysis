#!/bin/bash

# Software:		SAMtools
# Source:		http://samtools.sourceforge.net/
# Reference:	Li H. et al. (2009) The Sequence alignment/map (SAM) format and SAMtools.
				# Bioinformatics, 25, 2078-9.
				# Li H. A statistical framework for SNP calling, mutation discovery, association
	       		# mapping and population genetical parameter estimation from sequencing data.
	        	# Bioinformatics. 2011 Nov 1;27(21):2987-93
# Objective:	remove reads with mapping quality below 20, only include mapped reads in proper
				# pairs, export as binary bam files and sort reads with respect to chromosomal 
				# position

samtools view $INPUT \
	-q 20 \
	-f 2 \
	-u \
	-b | \
samtools sort - $OUTPUT

### VARIABLES:
# $INPUT:		name and path of mapped SAM file
# $OUTPUT:		name for mapped, sorted, compressed BAM output file

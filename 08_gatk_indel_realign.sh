#!/bin/bash

# Software:		Genome Analysis Toolkit
# Source:		https://software.broadinstitute.org/gatk/
# Reference:	DePristo M. et al. framework for variation discovery and genotyping using 
				# next-generation DNA sequencing data. 2011 NATURE GENETICS 43:491-498 
# Objective:	local realignment around INDELs supported by known INDELs in reference data



# STEP1: identification of regions which need realignemnt
java -Xmx6g -jar GATK.jar \
	-T RealignerTargetCreator \
	-R $REFERENCE \
	-I $INPUT \
	-o $OUTPUT.intervals \
	-known Mills_and_1000G_gold_standard.indels.b37.vcf \
	-known 1000G_phase1.indels.b37.vcf 
	
# STEP2: perform realignment in regions identified in previous steps
java -Xmx6g -jar GATK.jar \
	-T IndelRealigner \
	-R $REFERENCE.fasta \
	-I $INPUT \
	-o $OUTPUT.bam \
	-known Mills_and_1000G_gold_standard.indels.b37.vcf \
	-known 1000G_phase1.indels.b37.vcf \
	-targetIntervals $OUTPUT.intervals 

### VARIABLES:
# $INPUT:		name and path of mapped, sorted, compressed, merged, marked, covered BAM file
# $REFERENCE:	name and path to reference genome FASTA file
# $OUTPUT:		basename for output files including BAM file with realigned INDELs

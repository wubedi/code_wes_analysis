#!/bin/bash

# Software:		Burrows-Wheeler Aligner
# Source:		http://bio-bwa.sourceforge.net/
# Reference:	Li H. and Durbin R. (2009) Fast and accurate short read alignment with Burrows-
				# Wheeler Transform. Bioinformatics, 25:1754-60.
				# Li H. and Durbin R. (2010) Fast and accurate long-read alignment with Burrows-
				# Wheeler Transform. Bioinformatics, Epub. 
# Objective:	map paired-end reads to reference genome and export as sam files


bwa mem -t 8 $REFERENCE $INPUT1 $INPUT2 \
	-M \
	-c 20000 \
	-R "$READGROUP" > $OUTPUT.sam

### VARIABLES:
# $INPUT1:		name and path of filtered, trimmed FASTQ files, read1
# $INPUT2:		name and path of filtered, trimmed FASTQ files, read2 (paired read to INPUT1) 
# $OUTPUT:		basename for mapped SAM output file
# $READGROUP:	unique identifier for sequences containing meta-data as sample name,
				# lane identifier, infos about NGS chemistry, sequencer and sequencing facility

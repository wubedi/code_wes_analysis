#!/bin/bash

# Software:		Genome Analysis Toolkit
# Source:		https://software.broadinstitute.org/gatk/
# Reference:	DePristo M. et al. framework for variation discovery and genotyping using 
				# next-generation DNA sequencing data. 2011 NATURE GENETICS 43:491-498 
# Objective:	recalibration of base quality scores supported by reference data

# STEP1: find empirical accurate error model for base quality score recalibration
java -Xmx8g -jar GATK.jar \
	-T BaseRecalibrator \
	-I $INPUT \
	-R $REFERENCE \
	-o $OUTPUT.firstpass \
	-knownSites dbsnp_138.b37.vcf \
	-knownSites Mills_and_1000G_gold_standard.indels.b37.vcf
		
# STEP2: create new bam files with recalibrated base quality scores
java -Xmx8g -jar GATK.jar \
	-T PrintReads \
	-I $INPUT \
	-R $REFERENCE \
	-o $OUTPUT.final.bam \
	-BQSR $OUTPUT.firstpass

### VARIABLES:
# $INPUT:		name and path of mapped, sorted, compressed, merged, marked, covered, 
				# indel-realigned BAM file
# $REFERENCE:	name and path to reference genome FASTA file
# $OUTPUT:		basename for output files including final BAM file with recalibrated
				# base quality scores

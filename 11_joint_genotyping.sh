#!/bin/bash

# Software:		Genome Analysis Toolkit
# Source:		https://software.broadinstitute.org/gatk/
# Reference:	DePristo M. et al. framework for variation discovery and genotyping using 
				# next-generation DNA sequencing data. 2011 NATURE GENETICS 43:491-498 
# Objective:	merge and filter gvcf records from HaplotypeCaller and create vcf files

java -Xmx2g -jar GATK.jar \
	-T GenotypeGVCFs \
	-R $REFERENCE \
	--dbsnp dbsnp_138.b37.vcf \
	--variant $INPUT1.gvcf \
	--variant $INPUT2.gvcf \
	--variant $INPUT_N.gvcf \
	-o $OUTPUT.vcf

### VARIABLES:
# $INPUT1:		name and path of genomic VCF file for sample 1
# $INPUT2:		name and path of genomic VCF file for sample 2
# $INPUT_N:		name and path of genomic VCF file for sample N
# $REFERENCE:	name and path to reference genome FASTA file
# $OUTPUT:		basename for multi-sample VCF output file

#!/bin/bash

# Software:		Genome Analysis Toolkit
# Source:		https://software.broadinstitute.org/gatk/
# Reference:	DePristo M. et al. framework for variation discovery and genotyping using 
				# next-generation DNA sequencing data. 2011 NATURE GENETICS 43:491-498 
# Objective:	recalibrate variant quality scores for INDELs supported by literature data and filter
				# INDELs accordingly

# STEP1: fit Gaussian mixture model for variant recalibration
java -Xmx1g -jar GATK.jar \
	-T VariantRecalibrator \
	-R $REFERENCE \
	--input $INPUT \
	-resource:mills,known=true,training=true,truth=true,prior=12.0 $MILLS \
	-an DP -an QD -an MQRankSum -an ReadPosRankSum -an FS -an AF \
	-tranche 100.0 -tranche 99.9 -tranche 99.0 -tranche 95.0 -tranche 90.0 \
	--maxGaussians 4 \
	--mode INDEL \
	--badLodCutoff -5 \
	-minNumBad 1000 \
	-recalFile $OUTPUT.indel.recal \
	-tranchesFile $OUTPUT.indel.tranches \
	-log $OUTPUT.indel.recal.log \
	-rscriptFile $OUTPUT.indel.plots.R

# STEP2: create new vcf file with recalibrated quality scores
java -Xmx1g -jar GATK.jar \
	-T ApplyRecalibration \
	-R $REFERENCE \
	--input $INPUT \
	--ts_filter_level 99.0 \
	-mode INDEL \
	-recalFile $OUTPUT.indel.recal \
	-tranchesFile $OUTPUT.indel.tranches \
	-log $OUTPUT.indel.filter.log \
	-o $OUTPUT.raw.recalibrated.snp.indel.vcf

### VARIABLES:
# $INPUT:		name and path of multi-sample VCF file with recalibrated SNP quality scores
# $REFERENCE:	name and path to reference genome FASTA file
# $OUTPUT:		basename for output files as VCF file with recalibrated SNP and INDEL quality
				# scores and mulitple log files

#!/bin/bash

# Software:		SAMtools
# Source:		http://samtools.sourceforge.net/
# Reference:	Li H. et al. (2009) The Sequence alignment/map (SAM) format and SAMtools. 
				# Bioinformatics, 25, 2078-9.
				# Li H. A statistical framework for SNP calling, mutation discovery, association
				# mapping and population genetical parameter estimation from sequencing data.
				# Bioinformatics. 2011 Nov 1;27(21):2987-93
# Objective:	merge all reads from different lanes belonging to same biological sample

samtools merge $OUTPUT $INPUT1 $INPUT2 $INPUT_N

### VARIABLES:
# $INPUT1:		name and path of mapped, sorted, compressed BAM file for lane 1
# $INPUT2:		name and path of mapped, sorted, compressed BAM file for lane 2
# $INPUT_N:		name and path of mapped, sorted, compressed BAM file for lane N
# $OUTPUT:		name for mapped, sorted, compressed, merged BAM output file

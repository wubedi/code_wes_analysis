#!/bin/bash

# Software:		Genome Analysis Toolkit
# Source:		https://software.broadinstitute.org/gatk/
# Reference:	DePristo M. et al. framework for variation discovery and genotyping using 
				# next-generation DNA sequencing data. 2011 NATURE GENETICS 43:491-498 
# Objective:	identify active regions with deviations from reference genome and locally reassemble
				# haplotypes, calculate haplotypes likelihoods and assign genotypes

java -Xmx16g -jar GATK.jar \
	-T HaplotypeCaller \
	-I $INPUT \
	-R $REFERENCE \
	-D dbsnp_138.b37.vcf \
	-ERC GVCF \
	-variant_index_type LINEAR \
	-variant_index_parameter 128000 \
	-o $OUTPUT.gvcf

### VARIABLES:
# $INPUT:		name and path of final BAM file
# $REFERENCE:	name and path to reference genome FASTA file
# $OUTPUT:		basename for Genomics VCF output file
